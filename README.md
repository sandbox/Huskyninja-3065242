# Sherpa Webform

## Installation

Follow the instructions from "Installing Sandbox Modules" at https://www.drupal.org/docs/8/extending-drupal-8/installing-sandbox-modules

## Configuration

* Global settings for the module are found on the Sherpa Webform configuration page. The default values provided should be enough to begin testing in Sherpa's sandbox environment. To move to production you will need a Production URL, Company ID and Community ID provided by Sherpa.
* Webforms are configured individually from the Build menu under the Sherpa Webform Field Mapping tab. You can enable the form to send to Sherpa, set a custom Community ID and map the fields to their Sherpa Data Element value.
* For more information, see the module's help documentation.

## Version Information

0.8-beta:
- Added composer.json

0.7-beta:
- Added comments and formatted "the Drupal way"
- Changed some form field code for consistancy

0.6-beta:
- Cleaned up the admin page using fieldsets
- Cleaned up webform config page by hiding options when form is not selected to be sent to sherpa
- Finished Quick Set for use on a form by form basis (with global value)

0.5-beta:
- Added Sherpa's default ReferralDate & ReferralDateTime to Drupal's Date Formats
- Allow users to set ReferralDate & ReferralDateTime from admin page

0.4.1-beta:
- fixed 'Undefined index: actions_sherpa_field' issue
- work begun on advanced quickset options

0.4-beta:
- update readme
- update Webform settings form (conditional validation) & removed 'actions' buttons from mapping list
- Removed un-used JS and libraries.yml 

0.3-beta:
- Removed API Key from configuration & cURL header
- Changed the label name of Base URL to Sherpa API URL on the configuration screen
- Changed the page name of Sherpa Webform Field Mapping to Sherpa Configuration
- Added the ability to use Drupal's RESTful Web Services module
- Added additional entries logs for verbose logging setting
- Fixed spelling and grammar issues in the Help documents

0.2-beta:
- Added Quick Set function
- Added interface for individual Webform integration under Build menu
- Changed Webform selection from comma seperated list in main configuration to individual Webform checkbox
- Added field mapping and custom Community ID
- Added module help page and readme document
- Added framework for future scripting of interface elements under Sherpa Webform Field Mapping
- Added version number (I need to keep track with all these sites)

0.1-beta:
- Basic functionality with code mapped fields. Very primitive.

Things to do:
- Disable sending if all form fields are set without mapping even if send form to sherpa selected
- Allow selection of a data element limited to a single instance (ajax remove them as you go)
	- remove them if quickset is checked for the form
- Get correct sandbox company & community id for default values
- Allow for selection of v1 or v2 of sherpa and adjust config fields based on version (create a v2 of module?)
	- need correct sandbox values
- is webform_submission_presave() correct or should it be hook_webform_submission_actions($node, $submission) https://sites.google.com/site/ravivmane/webform-in-drupal/webform-hooks-example
